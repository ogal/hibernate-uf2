# hibernate-uf2

Proyecto perteneciente a la asignatura de M6 - UF2  

#### Herramientas utilizadas   
1. Eclipse
2. Servidor para la BBDD - Laragon (mysql 5.7)
	Creada una nueva base de datos con un usuario nuevo
3. Hibernate JAR files (5.4 stable) y JDBC Mysql Driver Connector

### To Do List  

* Crea un nuevo proyecto en Eclipse  
* Descarga los archivos de Hibernate -> [Hibernate downloads](http://hibernate.org/orm/releases/)  
* Creamos una carpeta en nuestro proyecto de Eclipse que llamaremos **lib**  
    -> en ella pegamos los archivos de Hibernate que encontramos en la carpeta _required_, dentro del zip que descargamos.  
* Descarga el driver connector para Mysql  
* Para Win10 descargammos el Conector que se identifica como SO: platform independent. [Descarga](https://dev.mysql.com/downloads/file/?id=484819)  
* Añade el Jar al proyecto en Eclipse -> Build Path  
* Una vez que tenemos los jars de Hibernate y el Connector de Mysql agregados al proyecto en la carpeta lib, lo que hacemos es agregarlos al build path


### 1. Testeando la Conexión de JDBC  

                         
~~~java
package com.luv2code.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {

		String jdbcUrl = "jdbc:mysql://localhost:3306/hb_student_tracker?serverTimezone=UTC";
		String user = "hbstudent";
		String pass = "hbstudent";
		
try {
	System.out.println("Connecting to database " + jdbcUrl);
			
	Connection myConn = 
		DriverManager.getConnection(jdbcUrl, user, pass);
			
	System.out.println("Conexion establecida");
			
} catch (Exception e) {
	System.out.println(e);
		}

	}

}
~~~

Probamos que funcione bien Hibernate en si mismo:  

Cosas que hacer:  

Añadir el archivo de configuracion
Sirve para especificar como Hibernate se debe conectar a la base de datos.
Pegamos el archivo directamente en la carpeta src.

Anotar o mapear las clases Java
Utilizamos Java Annotations (hacerlo con XML se considera obsoleto)
Mapeamos la tabla y sus respectivos campos.

http://www.techferry.com/articles/hibernate-jpa-annotations.html
