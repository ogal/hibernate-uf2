package com.luv2code.jdbc;
/**
 * Probamos que conecta con nuestra BBDD de Laragon
 * */
import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {
		
		String jdbcUrl = 
				"jdbc:mysql://localhost:3306/hb_student_tracker?serverTimezone=UTC";
		String user = "hbstudent";
		String pass = "hbstudent";
		
		Connection myConn = null;
		
		try {
			System.out.println("Connecting to database " + jdbcUrl);
			
			myConn = DriverManager.getConnection(jdbcUrl, user, pass);
			
			System.out.println("Conexion establecida");
			
			myConn.close();
			
		} catch (Exception e) {
			System.out.println(e);
			
		} 

	}

}
