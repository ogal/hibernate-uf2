package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class QueryStudentDemo {

	public static void main(String[] args) {
		
//		create session factory (1 sola vez)
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml") // Tomamos el archivo de configuracion de la ra�z
				.addAnnotatedClass(Student.class) // Importamos la clase student, modelo de la tabla
				.buildSessionFactory(); // construimos el factory
		
//		creamos la session
		Session session = factory.getCurrentSession();
		
		try {
//			start transaction
			session.beginTransaction();
			
//			hacemos la query, nos da una lista con todos los Objetos Student
			List <Student> theStudents = session.createQuery("from Student").getResultList();	
			
//			muestra los registros
			displayStudents(theStudents);
			
//			CONSULTA 1 - Obten el estudiante cuyo apellido es DOE 
			theStudents = session.createQuery("from Student s where s.lastName='Doe'").getResultList();
			
			System.out.println("\n Estudiante cuyo apellido es DOE\n");
			displayStudents(theStudents);
			
//			CONSULTA 2 - lastName = Doe OR firstName 
			theStudents = session.createQuery("from Student s where s.lastName='Doe' OR s.firstName='Liam'").getResultList();
			
			System.out.println("\n estudiantes de apellido Doe o nombre Liam\n");
			displayStudents(theStudents);
			
//			commit transaction
			session.getTransaction().commit();
			
			System.out.println("HECHO!!");
			
		} catch (Exception e) {

			factory.close();
		}


	}

	private static void displayStudents(List<Student> theStudents) {
		for(Student tempStudent : theStudents) {
			System.out.println(tempStudent);
		}
	}

}
