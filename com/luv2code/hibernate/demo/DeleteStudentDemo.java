package com.luv2code.hibernate.demo;
/**
 * Insertamos un nuevo registro en la tabla y acto seguido lo leemos por consola
 * */
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class DeleteStudentDemo {

	public static void main(String[] args) {
		
//		create session factory (1 sola vez por cada una de las conexiones a una BBDD)
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		
//		creamos la session
		Session session = factory.getCurrentSession();
		
		try {
			int studentId = 6;
			
//			Empezamos una nueva sesion y empezamos un nuevo Transaction
			session = factory.getCurrentSession();
			session.beginTransaction();
			
//			Tomamos el estudiante que hemos introducido previamente utilizando su ID
			Student miEstudiante = session.get(Student.class, studentId);
			System.out.println("Se ha tomado correctamente el estudiante: " + miEstudiante);
		
//			Eliminamos el estudiante
			//session.delete(miEstudiante);
			
//			Eliminamos un nuevo registro de forma alternativa
			session.createQuery("delete from Student where id=3").executeUpdate();
			
//			Comiteamos la transacción: 
			session.getTransaction().commit();
			
			System.out.println("HECHO!!");
			
		} catch (Exception e) {

			factory.close();
		}

	}

}
