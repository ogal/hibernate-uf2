package com.luv2code.hibernate.demo;
/**
 * Guardamos tres nuevos estudiantes en nuestra base de datos
 * */
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class PrimaryKeyDemo {

	public static void main(String[] args) {
		
//		create session factory (1 sola vez)
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		
//		creamos la session
		Session session = factory.getCurrentSession();
		
		try {
//			create 3 student object
			System.out.println("Creando 3 nuevos objetos estudiante...");
			Student tempStudent1 = new Student("Lucia", "Doe", "luci@doe.com");
			Student tempStudent2 = new Student("Liam", "Public", "liam@public.com");
			Student tempStudent3 = new Student("Paul", "Smith", "paul@smith.com");
			
//			start transaction
			session.beginTransaction();
			
//			save the student object
			System.out.println("Guardando el registro....");
			session.save(tempStudent1);
			session.save(tempStudent2);
			session.save(tempStudent3);
			
//			commit transaction
			session.getTransaction().commit();
			
			System.out.println("Registro guardado!!");
			
		} catch (Exception e) {

			factory.close();
		}
		
	}

}
