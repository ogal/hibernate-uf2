package com.luv2code.hibernate.demo;
/**
 * Insertamos un nuevo registro en la tabla y acto seguido lo leemos por consola
 * */
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class ReadStudentDemo {

	public static void main(String[] args) {
		
//		create session factory (1 sola vez por cada una de las conexiones a una BBDD)
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		
//		creamos la session
		Session session = factory.getCurrentSession();
		
		try {
//			create a student object
			System.out.println("Creando un nuevo objeto estudiante...");
			Student tempStudent = new Student("Sara", "Jean", "saraJean@codigojava.com");
			
//			start transaction
			session.beginTransaction();
			
//			save the student object
			System.out.println("Guardando el registro...." + tempStudent);
			session.save(tempStudent);
			
//			commit transaction
			session.getTransaction().commit();
			
//			NUEVO CODIGO PARA TOMAR DATOS DESDE LA BBDD
			System.out.println("Se ha guardado el estudiante: " + tempStudent.getId());
			
//			Empezamos una nueva sesion y empezamos un nuevo Transaction
			session = factory.getCurrentSession();
			session.beginTransaction();
			
//			Tomamos el estudiante que hemos introducido previamente utilizando su ID
			Student miEstudiante = session.get(Student.class, tempStudent.getId());
			System.out.println("Se ha tomado correctamente el estudiante: " + miEstudiante);
			
//			Comiteamos la transacción: 
			session.getTransaction().commit();
			
			System.out.println("HECHO!!");
			
		} catch (Exception e) {

			factory.close();
		}

	}

}
