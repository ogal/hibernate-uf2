package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args) {
		
//		create session factory (1 sola vez)
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		
//		creamos la session
		Session session = factory.getCurrentSession();
		
		try {
//			create a student object
			System.out.println("Creando un nuevo objeto estudiante...");
			Student tempStudent = new Student("Oscar", "Queloque", "oscar@que.com");
			
//			start transaction
			session.beginTransaction();
			
//			save the student object
			System.out.println("Guardando el registro....");
			session.save(tempStudent);
			
//			commit transaction
			session.getTransaction().commit();
			
			System.out.println("Registro guardado!!");
			
		} catch (Exception e) {

			factory.close();
		}

	}

}
