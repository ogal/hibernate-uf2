package com.luv2code.hibernate.demo;
/**
 * Insertamos un nuevo registro en la tabla y acto seguido lo leemos por consola
 * */
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class UpdateStudentDemo {

	public static void main(String[] args) {
		
//		create session factory (1 sola vez por cada una de las conexiones a una BBDD)
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		
//		creamos la session
		Session session = factory.getCurrentSession();
		
		try {
			int studentId = 1;
			
//			Empezamos una nueva sesion y empezamos un nuevo Transaction
			session = factory.getCurrentSession();
			session.beginTransaction();
			
//			Tomamos el estudiante que hemos introducido previamente utilizando su ID
			Student miEstudiante = session.get(Student.class, studentId);
			System.out.println("Se ha tomado correctamente el estudiante: " + miEstudiante);
			
			System.out.println("Actualizando estudiante...");
			miEstudiante.setFirstName("Scooby");
			
//			Comiteamos la transacción: 
			session.getTransaction().commit();
			
//			NUEVA TRANSACCIÓN Y NUEVA ACTUALIZACION DE CAMPOS DE LA TABLA SIN CREAR EL OBJETO ESTUDIANTE
			
//			Tomamos una nueva sesion para una nueva actualizacion
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			System.out.println("Actualizando los registros de la tabla Estudiante");
			
			session.createQuery("update Student set email='correo@actualizado.com' where id=5").executeUpdate();
			
//			Comiteamos la transacción: 
			session.getTransaction().commit();
			
			
			System.out.println("HECHO!!");
			
		} catch (Exception e) {

			factory.close();
		}

	}

}
